package com.supericy.level;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.util.pathfinding.Path;

import com.supericy.Camera;
import com.supericy.misc.SolidFill;

public class Road
{

	private Image unbuildableMask;
	
	private int width;
	private int height;
	
	private Path path;
	
	public Road(Image unbuildableMask, Path path)
	{
		this.unbuildableMask = unbuildableMask;
		
		this.width = unbuildableMask.getWidth();
		this.height = unbuildableMask.getHeight();
		
		this.path = path;
	}
	
	public void renderPath(Graphics g, Camera camera)
	{
		if (path.getLength() > 1)
		{
			Path.Step step = path.getStep(0);
			Path.Step next;
			
			for (int i = 1; i < path.getLength(); i++)
			{
				next = path.getStep(i);
				
				float translatedX = camera.translateX(step.getX());
				float translatedY = camera.translateY(step.getY());
				
				Line line = new Line(translatedX, translatedY, camera.translateX(next.getX()), camera.translateY(next.getY()));
				
				float[] norm = line.getNormal(0);
				
				Polygon poly = new Polygon();
				
				float triangleWidth = 15f;
				float triangleHeight = 0.05f;
				
				poly.addPoint(line.getX1() + line.getDX() * triangleHeight - norm[0] * triangleWidth, line.getY1() + line.getDY() * triangleHeight - norm[1] * triangleWidth);
				poly.addPoint(line.getX1() + line.getDX() * triangleHeight + norm[0] * triangleWidth, line.getY1() + line.getDY() * triangleHeight + norm[1] * triangleWidth);
				poly.addPoint(line.getX2() - (line.getDX() * triangleHeight), line.getY2() - (line.getDY() * triangleHeight));

				g.draw(line, SolidFill.black);
				g.fill(poly, SolidFill.red);
				
				step = next;
			}
		}
	}
	
	public Path.Step getWaypoint(int index)
	{
		return path.getStep(index);
	}
	
	public int getNumWaypoints()
	{
		return path.getLength();
	}

	public boolean isRoad(int x, int y)
	{
		boolean inBounds = false;
		
		if (x >= 0 && x <= width && y >= 0 && y <= height)
			inBounds = true;
		
		return inBounds && unbuildableMask.getColor(x, y).getAlpha() != 0;
	}
	
}
