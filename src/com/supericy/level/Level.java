package com.supericy.level;

import java.util.List;

import org.newdawn.slick.Image;

import com.supericy.enemy.EnemyManager;
import com.supericy.level.bank.Bank;
import com.supericy.tower.Tower;
import com.supericy.tower.TowerManager;

public class Level
{

	public static final String	DATA_PATH	= "data/levels/";

	private Image				background;

	private int					worldWidth;
	private int					worldHeight;

	private TowerManager		towerManager;
	private EnemyManager		enemyManager;

	public Level(Image background, int worldWidth, int worldHeight, TowerManager towerManager, EnemyManager enemyManager)
	{
		this.background = background;

		this.worldWidth = worldWidth;
		this.worldHeight = worldHeight;

		this.towerManager = towerManager;
		this.enemyManager = enemyManager;
	}

	public Bank getBank()
	{
		return towerManager.getBank();
	}

	public int getWorldWidth()
	{
		return worldWidth;
	}

	public int getWorldHeight()
	{
		return worldHeight;
	}

	public Image getBackground()
	{
		return background;
	}

	public TowerManager getTowerManager()
	{
		return towerManager;
	}

	public List<Tower> getBuiltTowers()
	{
		return towerManager.getBuiltTowers();
	}

}
