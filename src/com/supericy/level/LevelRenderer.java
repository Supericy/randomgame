package com.supericy.level;

import org.newdawn.slick.GameContainer;

import com.supericy.AbstractRenderer;
import com.supericy.Camera;

public class LevelRenderer extends AbstractRenderer<Level>
{

	public LevelRenderer(GameContainer gc, Camera camera)
	{
		super(gc, camera);
	}

	public void render(Level level)
	{
		level.getBackground().draw(-camera.getX(), -camera.getY());
	}

}
