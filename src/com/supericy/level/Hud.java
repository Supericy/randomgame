package com.supericy.level;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import com.supericy.level.bank.Bank;
import com.supericy.tower.Tower;

public class Hud
{

	private static int	TOWER_PADDING	= 8;

	private float		x;
	private float		y;
	private int			width;
	private int			height;

	private Bank		bank;
	private List<Tower>	towers;

	private int			towerPosX		= 60;

	public Hud(float x, float y, int width, int height, Bank bank, List<Tower> towers)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		this.bank = bank;
		this.towers = towers;

		for (Tower tower : towers)
		{
			// TODO make these towers not shoot
			
			tower.setX(x + towerPosX);
			towerPosX += tower.getWidth() + TOWER_PADDING;

			tower.setY(y);
		}
	}

	public void setBank(Bank bank)
	{
		this.bank = bank;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public void render(Graphics g)
	{
		g.getFont().drawString(0, y, "$" + bank.getBalance(), Color.yellow);

		for (Tower tower : towers)
			tower.getSprite().draw(tower.getX(), tower.getY(), tower.getWidth(), tower.getHeight());
	}

}
