package com.supericy.level;

import org.newdawn.slick.Image;
import org.newdawn.slick.util.pathfinding.Path;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.supericy.AbstractFactory;
import com.supericy.enemy.EnemyFactory;
import com.supericy.enemy.EnemyManager;
import com.supericy.level.bank.Bank;
import com.supericy.tower.TowerFactory;
import com.supericy.tower.TowerManager;

public class LevelFactory extends AbstractFactory<Level>
{
	
	public Level create(String levelName)
	{
		final String LEVEL_FOLDER = Level.DATA_PATH + levelName + "/";
	
		Level level = null;
		
		try
		{
			Document doc = loadXML(LEVEL_FOLDER + "/data.xml");
			
			// get the level element
			Element config = (Element) doc.getElementsByTagName("level").item(0);
			
			// read in the variables
			int money = readInteger(config, "money");
			Image background = new Image(LEVEL_FOLDER + readString(config, "background"));
			Image unbuildableMask = new Image(LEVEL_FOLDER + readString(config, "unbuildableMask"));
			int worldWidth = readInteger(config, "worldWidth");
			int worldHeight = readInteger(config, "worldHeight");
			
			// start wiring the level
			Bank bank = new Bank(money);
			
			Road road = new Road(unbuildableMask, parsePath(readString(config, "path")));

			TowerManager towerManager = new TowerManager(new TowerFactory(), bank, road);
			EnemyManager enemyManager = new EnemyManager(new EnemyFactory());

			level = new Level(background, worldWidth, worldHeight, towerManager, enemyManager);
		}
		catch (Exception e)
		{
//			System.err.println("Failed to load level " + levelNumber + ": " + e.getMessage());
			
			e.printStackTrace();
		}
		
		return level;
	}
	
	// each path coord seperated by a semicolen
	private static Path parsePath(String csv)
	{
		Path path = new Path();
		
		String[] tokens = csv.split(";");
		
		for (String token : tokens)
		{
			String[] coords = token.trim().split(",");
			
			int x = Integer.parseInt(coords[0].trim());
			int y = Integer.parseInt(coords[1].trim());
			
			path.appendStep(x, y);
		}
		
		return path;
	}
	
}
