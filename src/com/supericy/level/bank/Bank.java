package com.supericy.level.bank;


public class Bank
{
	
	private int balance;
	
	public Bank()
	{
		this(0);
	}
	
	public Bank(int balance)
	{
		this.balance = balance;
	}
	
	public void deposit(int amount)
	{
		balance += amount;
	}
	
	public void withdraw(int amount) throws NotEnoughMoneyException
	{
		if (amount > balance)
			throw new NotEnoughMoneyException("You only have " + balance + " in your bank account!");
		
		balance -= amount;
	}
	
	public int getBalance()
	{
		return balance;
	}
	
}
