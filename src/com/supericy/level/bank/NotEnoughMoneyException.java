package com.supericy.level.bank;

public class NotEnoughMoneyException extends Exception
{

	private static final long	serialVersionUID	= -5207275507265392761L;
	
	public NotEnoughMoneyException(String string)
	{
		super(string);
	}

}
