package com.supericy.enemy;

import org.newdawn.slick.GameContainer;

import com.supericy.AbstractRenderer;
import com.supericy.Camera;

public class EnemyRenderer extends AbstractRenderer<Enemy>
{

	public EnemyRenderer(GameContainer gc, Camera camera)
	{
		super(gc, camera);
	}

	public void render(Enemy enemy)
	{
		enemy.getBackground().draw(camera.translateX(enemy.getCenterX()), camera.translateY(enemy.getCenterY()));
	}
	
}
