package com.supericy.enemy;

import org.newdawn.slick.Image;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.supericy.AbstractFactory;

public class EnemyFactory extends AbstractFactory<Enemy>
{

	public Enemy create(String enemyName)
	{
		final String ENEMY_FOLDER = Enemy.DATA_PATH + enemyName + "/";
		
		Enemy enemy = null;
		
		try
		{
			Document doc = loadXML(ENEMY_FOLDER + "/data.xml");
			
			Element config = (Element) doc.getElementsByTagName("enemy").item(0);
			
			String type = readString(config, "type");
			int width = readInteger(config, "width");
			int height = readInteger(config, "height");
			Image sprite = new Image(ENEMY_FOLDER + readString(config, "sprite"));
			int health = readInteger(config, "health");
			float speed = readFloat(config, "speed");
			
			enemy = new Enemy(type, 0, 0, width, height, sprite, health, speed, null);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return enemy;
	}
	
}
