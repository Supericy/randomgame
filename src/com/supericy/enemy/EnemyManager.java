package com.supericy.enemy;

import com.supericy.AbstractFactory;
import com.supericy.AbstractManager;

public class EnemyManager extends AbstractManager<Enemy>
{

	public EnemyManager(AbstractFactory<Enemy> factory)
	{
		super(factory);
	}

	public void spawn(String type, float x, float y)
	{
		Enemy enemy;

		try
		{
			enemy = factory.create(type);
			enemy.setX(x);
			enemy.setY(y);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
