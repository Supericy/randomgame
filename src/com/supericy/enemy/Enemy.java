package com.supericy.enemy;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public class Enemy
{

	public static final String DATA_PATH = "data/enemies/";
	
	private static int ENEMY_COUNT = 0;
	
	private final int id;
	
	private float x;
	private float y;
	private int width;
	private int height;
	private Image sprite;
	private int health;
	private float speed;
	private Ability[] abilities;
	
	private Rectangle body;
	
	public Enemy(String type, float x, float y, int width, int height, Image sprite, int health, float speed, Ability[] abilities)
	{
		this.id = ENEMY_COUNT++;
		
//		this.x = x;
//		this.y = y;
//		this.width = width;
//		this.height = height;
		this.sprite = sprite;
		this.health = health;
		this.speed = speed;
		this.abilities = abilities;
		
		body = new Rectangle(x, y, width, height);
	}
	
	public void setX(float x)
	{
		body.setX(x);
	}
	
	public void setY(float y)
	{
		body.setY(y);
	}
	
	public float getWidth()
	{
		return body.getWidth();
	}
	
	public float getHeight()
	{
		return body.getHeight();
	}
	
	public float getCenterX()
	{
		return body.getCenterX();
	}
	
	public float getCenterY()
	{
		return body.getCenterY();
	}
	
	public Image getBackground()
	{
		return sprite;
	}
	
	public int getID()
	{
		return id;
	}
	
	public float getX()
	{
		return body.getX();
	}
	
	public float getY()
	{
		return body.getY();
	}
	
	public float getSpeed()
	{
		return speed;
	}
	
}
