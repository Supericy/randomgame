package com.supericy.tower;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public class Tower
{

	public static final String	DATA_PATH				= "data/towers/";
	public static final Color	DEFAULT_BORDER_COLOR	= Color.gray;

	public final String			type;

	private float				x;
	private float				y;

	private int					width;
	private int					height;

	private Image				sprite;
	private Rectangle			border;

	private int					price;
	private int					range;
	private int					attackspeed;
	private Attack				attack;

	private boolean				buildableOnRoad;
	private boolean				buildableOffRoad;

	private Color				borderColor;

	private boolean				disabled				= false;

	public Tower(String type, float x, float y, int width, int height, Image sprite, int price, int range, int attackspeed, Attack attack,
			boolean buildableOnRoad, boolean buildableOffRoad)
	{
		this.type = type;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.sprite = sprite;
		this.price = price;
		this.range = range;
		this.attackspeed = attackspeed;
		this.attack = attack;
		this.buildableOnRoad = buildableOnRoad;
		this.buildableOffRoad = buildableOffRoad;

		// create a border rectangle
		this.border = new Rectangle(x, y, width, height);
		this.borderColor = DEFAULT_BORDER_COLOR;
	}

	public void disable(boolean disabled)
	{
		this.disabled = disabled;
	}

	public boolean isBuildableOnRoad()
	{
		return buildableOnRoad;
	}

	public boolean isBuildableOffRoad()
	{
		return buildableOffRoad;
	}

	public Rectangle getBorder()
	{
		return border;
	}

	public int getRange()
	{
		return range;
	}

	public Color getBorderColor()
	{
		return borderColor;
	}

	public void setBorderColor(Color color)
	{
		this.borderColor = color;
	}

	public int getPrice()
	{
		return price;
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public void setX(float x)
	{
		this.x = x;

		this.border.setX(x);
	}

	public void setY(float y)
	{
		this.y = y;

		this.border.setY(y);
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Image getSprite()
	{
		return sprite;
	}

	public String getType()
	{
		return type;
	}

}
