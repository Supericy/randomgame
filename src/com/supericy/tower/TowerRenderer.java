package com.supericy.tower;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;

import com.supericy.AbstractRenderer;
import com.supericy.Camera;
import com.supericy.misc.SolidFill;

public class TowerRenderer extends AbstractRenderer<Tower>
{

	// temp shapes for rendering tower borders/rangeIndicators
	private Rectangle	border;
	private Circle		rangeIndicator;

	private boolean		renderBorders			= false;
	private boolean		renderRangeIndicators	= false;

	public TowerRenderer(GameContainer gc, Camera camera)
	{
		super(gc, camera);

		// temp objects to render borders (move these around rather than
		// creating new objects)
		this.border = new Rectangle(0, 0, 0, 0);
		this.rangeIndicator = new Circle(0, 0, 0);
	}

	public void renderBorders(boolean renderBorders)
	{
		this.renderBorders = renderBorders;
	}
	
	public void renderRangeIndicators(boolean renderRangeIndicators)
	{
		this.renderRangeIndicators = renderRangeIndicators;
	}

	public void render(Tower tower)
	{
		if (tower != null)
		{
			float x = tower.getX();
			float y = tower.getY();
			int width = tower.getWidth();
			int height = tower.getHeight();
			Image background = tower.getSprite();

			background.draw(-camera.getX() + x, -camera.getY() + y, width, height);

			if (renderBorders)
				renderBorder(tower);
		}
	}

	public void renderBorder(Tower tower)
	{
		if (tower != null)
		{
			border.setX(-camera.getX() + tower.getX());
			border.setY(-camera.getY() + tower.getY());

			border.setWidth(tower.getWidth());
			border.setHeight(tower.getHeight());

			gc.getGraphics().draw(border, new SolidFill(tower.getBorderColor()));
		}
	}

	public void renderRangeIndicator(Tower tower)
	{
		if (tower != null)
		{
			rangeIndicator.setCenterX(-camera.getX() + (tower.getX() + tower.getWidth() / 2));
			rangeIndicator.setCenterY(-camera.getY() + (tower.getY() + tower.getHeight() / 2));

			rangeIndicator.setRadius(tower.getRange());

			gc.getGraphics().draw(rangeIndicator, new SolidFill(Color.white));
		}
	}

}
