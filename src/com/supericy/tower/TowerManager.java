package com.supericy.tower;

import java.util.List;

import org.newdawn.slick.Color;
import com.supericy.AbstractFactory;
import com.supericy.AbstractManager;
import com.supericy.level.Road;
import com.supericy.level.bank.Bank;

public class TowerManager extends AbstractManager<Tower>
{

	private Tower	ghost;
	private Bank	bank;
	private Road	road;

	public TowerManager(AbstractFactory<Tower> factory, Bank bank, Road road)
	{
		super(factory);

		this.bank = bank;
		this.road = road;
	}

	public Bank getBank()
	{
		return bank;
	}
	
	public Tower getGhost()
	{
		return ghost;
	}
	
	public String getGhostType()
	{
		return ghost.getType();
	}

	public void setGhostType(String type)
	{
		if (type == null)
		{
			ghost = null;
		}
		else
		{
			ghost = factory.create(type);
			ghost.getSprite().setAlpha(0.6f);
		}
		
		setGhostBuildable(false);
	}

	public void setGhostX(float x)
	{
		if (ghost != null)
			ghost.setX(x);
	}

	public void setGhostY(float y)
	{
		if (ghost != null)
			ghost.setY(y);
	}

	public void build(String type, float x, float y)
	{
		Tower tower;

		try
		{
			tower = factory.create(type);
			tower.setX(x);
			tower.setY(y);

			if (canBuildAt(tower, x, y) && bank.getBalance() >= tower.getPrice())
			{
				System.out.printf("new %s at %3d:%3d\n", tower.getType(), (int) tower.getX(), (int) tower.getY());

				bank.withdraw(tower.getPrice());

				entities.add(tower);
			}
		}
		catch (Exception e)
		{
			System.err.println("error: " + e.getMessage());
		}
	}

	// set if the ghost's colors for if it can built at it's current location
	public void setGhostBuildable(boolean buildable)
	{
		if (ghost != null)
		{
			if (buildable)
			{
				ghost.getSprite().setImageColor(255, 0, 0);
				ghost.setBorderColor(Tower.DEFAULT_BORDER_COLOR);
			}
			else
			{
				ghost.getSprite().setImageColor(255, 255, 255);
				ghost.setBorderColor(Color.red);
			}
		}
	}

	// if the ghost tower is in a position to be built
	public boolean canBuildAt(Tower tower, float x, float y)
	{
		if (!tower.isBuildableOffRoad() && !tower.isBuildableOnRoad())
			return false;
		
		for (int i = (int) tower.getX(); i < tower.getX() + tower.getWidth(); i++)
		{
			for (int j = (int) tower.getY(); j < tower.getY() + tower.getHeight(); j++)
			{
				boolean isRoad = road.isRoad(i, j);
				
				if (!tower.isBuildableOnRoad())
				{
					if (isRoad)
						return false;
				}
				
				if (!tower.isBuildableOffRoad())
				{
					if (!isRoad)
						return false;
				}
			}
		}

		for (Tower other : entities)
		{
			if (tower.getBorder().intersects(other.getBorder()))
				return false;
		}

		return true;
	}

	public List<Tower> getBuiltTowers()
	{
		return entities;
	}

}
