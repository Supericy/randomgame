package com.supericy.tower;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Image;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.supericy.AbstractFactory;

public class TowerFactory extends AbstractFactory<Tower>
{
	
	public List<Tower> create(String[] towerNames)
	{
		List<Tower> towers = new ArrayList<Tower>();
		
		for (String towerName : towerNames)
			towers.add(create(towerName));
		
		return towers;
	}
	
	public Tower create(String towerName)
	{
		final String TOWER_FOLDER = Tower.DATA_PATH + towerName + "/";
		
		Tower tower = null;
		
		try
		{
			Document doc = loadXML(TOWER_FOLDER + "/data.xml");
			
			Element config = (Element) doc.getElementsByTagName("tower").item(0);
			
			String type = readString(config, "type");
			Image sprite = new Image(TOWER_FOLDER + readString(config, "sprite"));
			int width = readInteger(config, "width");
			int height = readInteger(config, "height");
			int price = readInteger(config, "price");
			int range = readInteger(config, "range");
			int attackspeed = readInteger(config, "attackspeed");
			boolean buildableOnRoad = readBoolean(config, "buildableOnRoad");
			boolean buildableOffRoad = readBoolean(config, "buildableOffRoad");
			
			tower = new Tower(type, 0, 0, width, height, sprite, price, range, attackspeed, null, buildableOnRoad, buildableOffRoad);
		}
		catch (Exception e)
		{
			e.printStackTrace();
//			System.err.println("Failed to create a " + towerName + ": " + e.getMessage());
		}
		
		return tower;
	}
	
}
