package com.supericy;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;

public class Camera
{

	private final float	speed	= 0.9f;

//	private float		x;
//	private float		y;

//	private int			top;
//	private int			bottom;
//	private int			left;
//	private int			right;

	private Rectangle	worldview;
	private Rectangle	view;

	public Camera(float x, float y, int width, int height, int worldWidth, int worldHeight)
	{
		this.view = new Rectangle(x, y, width, height);
		this.worldview = new Rectangle(0, 0, worldWidth, worldHeight);
	}
	
	public void setWorldWidth(int worldWidth)
	{
		this.worldview.setWidth(worldWidth);
	}
	
	public void setWorldHeight(int worldHeight)
	{
		this.worldview.setHeight(worldHeight);
	}

	public float translateX(float x)
	{
		return x - this.view.getX();
	}

	public float translateY(float y)
	{
		return y - this.view.getY();
	}

	public float getX()
	{
		return view.getX();
	}

	public float getY()
	{
		return view.getY();
	}

	public void update(GameContainer gc, int dt)
	{
		Input input = gc.getInput();

		float vel = speed * dt;
		
		float offsetX = 0;
		float offsetY = 0;
		
		if (input.isKeyDown(Input.KEY_RIGHT))
		{
			offsetX = vel;
		}

		if (input.isKeyDown(Input.KEY_LEFT))
		{
			offsetX = -vel;
		}

		if (input.isKeyDown(Input.KEY_UP))
		{
			offsetY = -vel;
		}

		if (input.isKeyDown(Input.KEY_DOWN))
		{
			offsetY = vel;
		}
		
		view.setX(view.getX() + offsetX);
		view.setY(view.getY() + offsetY);

		if (view.getMinX() < worldview.getMinX())
			view.setX(worldview.getMinX());

		if (view.getMaxX() > worldview.getMaxX())
			view.setX(worldview.getMaxX() - view.getWidth());

		if (view.getMinY() < worldview.getMinY())
			view.setY(worldview.getMinY());

		if (view.getMaxY() > worldview.getMaxY())
			view.setY(worldview.getMaxY() - view.getHeight());
	}

}
