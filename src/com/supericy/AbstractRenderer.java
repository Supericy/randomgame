package com.supericy;

import org.newdawn.slick.GameContainer;

public abstract class AbstractRenderer<T>
{

	protected final GameContainer	gc;
	protected final Camera			camera;

	public AbstractRenderer(GameContainer gc, Camera camera)
	{
		this.gc = gc;
		this.camera = camera;
	}
	
	public abstract void render(T something);

}
