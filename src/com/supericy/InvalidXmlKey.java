package com.supericy;

public class InvalidXmlKey extends Exception
{

	private static final long	serialVersionUID	= 6822147301907384975L;

	public InvalidXmlKey(String keyName)
	{
		super("no node exists with the name: " + keyName);
	}
	
}
