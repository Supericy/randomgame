package com.supericy.misc;

import org.newdawn.slick.Color;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

public class SolidFill implements ShapeFill
{

	public static final SolidFill white = new SolidFill(Color.white);
	public static final SolidFill black = new SolidFill(Color.black);
	public static final SolidFill red = new SolidFill(Color.red);
	public static final SolidFill yellow = new SolidFill(Color.yellow);
	
	private Color color;
	private Vector2f offset;
	
	public SolidFill(Color color)
	{
		this.color = color;
		this.offset = new Vector2f(0, 0);
	}
	
	@Override
	public Color colorAt(Shape shape, float x, float y)
	{
		return color;
	}

	@Override
	public Vector2f getOffsetAt(Shape shape, float x, float y)
	{
		return offset;
	}

}
