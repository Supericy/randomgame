package com.supericy;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public abstract class AbstractFactory<T>
{

	protected Document loadXML(String file) throws ParserConfigurationException, SAXException, IOException 
	{
		File fXmlFile = new File(file);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		
		Document doc = dBuilder.parse(fXmlFile);

		return doc;
	}
	
	protected String readString(Element parent, String name) throws InvalidXmlKey
	{
		NodeList nodes = parent.getElementsByTagName(name);
		
		if (nodes.getLength() == 0)
			throw new InvalidXmlKey(name);
		
		return nodes.item(0).getTextContent();
	}

	protected int readInteger(Element parent, String name) throws InvalidXmlKey
	{
		return Integer.parseInt(readString(parent, name));
	}
	
	protected boolean readBoolean(Element parent, String name) throws InvalidXmlKey
	{
		return Boolean.parseBoolean(readString(parent, name));
	}
	
	protected float readFloat(Element parent, String name) throws InvalidXmlKey
	{
		return Float.parseFloat(readString(parent, name));
	}
	
	public abstract T create(String type);
	
}
