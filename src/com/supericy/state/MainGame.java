package com.supericy.state;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.supericy.Camera;
import com.supericy.enemy.Enemy;
import com.supericy.enemy.EnemyFactory;
import com.supericy.enemy.EnemyRenderer;
import com.supericy.level.Hud;
import com.supericy.level.Level;
import com.supericy.level.LevelFactory;
import com.supericy.level.LevelRenderer;
import com.supericy.tower.Tower;
import com.supericy.tower.TowerFactory;
import com.supericy.tower.TowerManager;
import com.supericy.tower.TowerRenderer;

public class MainGame extends BasicGameState
{

	public static final GameState	STATE		= GameState.MAIN_GAME;

	private Camera					camera;
	private Hud						hud;
	private Level					level;
	private LevelFactory			levelFactory;
	private TowerFactory			towerFactory;
	private EnemyFactory			enemyFactory;

	private LevelRenderer			levelRenderer;
	private TowerRenderer			towerRenderer;
	private EnemyRenderer			enemyRenderer;

	private List<Tower>				selectableTowers;

	private List<Enemy>				enemies		= new ArrayList<Enemy>();

	private int						elapsedTime	= 0;

	private Enemy					enemy;

	@Override
	public void init(final GameContainer gc, StateBasedGame game) throws SlickException
	{
		// factories
		levelFactory = new LevelFactory();
		towerFactory = new TowerFactory();
		enemyFactory = new EnemyFactory();

		// shared dependencies
		selectableTowers = towerFactory.create(new File(Tower.DATA_PATH).list());
		hud = new Hud(0, gc.getHeight() - 100, gc.getWidth(), 100, null, selectableTowers);
		camera = new Camera(0, 0, gc.getWidth(), gc.getHeight(), 0, 0);

		// renderers
		levelRenderer = new LevelRenderer(gc, camera);
		towerRenderer = new TowerRenderer(gc, camera);
		enemyRenderer = new EnemyRenderer(gc, camera);

		// load level 1
		level = levelFactory.create("1");
		camera.setWorldWidth(level.getWorldWidth());
		camera.setWorldHeight(level.getWorldHeight());
		hud.setBank(level.getBank());

		level.getTowerManager().setGhostType(null);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException
	{
		levelRenderer.render(level);

		for (Tower tower : level.getBuiltTowers())
			towerRenderer.render(tower);

		for (Enemy enemy : enemies)
			enemyRenderer.render(enemy);

		towerRenderer.render(level.getTowerManager().getGhost());

		hud.render(g);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int dt) throws SlickException
	{
		camera.update(gc, dt);

		Input input = gc.getInput();

		int mouseX = input.getAbsoluteMouseX();
		int mouseY = input.getAbsoluteMouseY();
		boolean mousePrimaryPressed = input.isMousePressed(Input.MOUSE_LEFT_BUTTON);
		boolean recentlySelected = false; // prevent the tower from being built when you select it

		// TOWER MANAGER
		TowerManager towerManager = level.getTowerManager();
		Tower ghost = towerManager.getGhost();

		if (mousePrimaryPressed)
		{
			for (Tower tower : selectableTowers)
			{
				if (new Rectangle(tower.getX(), tower.getY(), tower.getWidth(), tower.getHeight()).contains(mouseX, mouseY))
				{
					towerManager.setGhostType(tower.getType());
					
					recentlySelected = true;
				}
			}
		}

		if (ghost != null)
		{
			int ghostX = (int) (camera.getX() + mouseX - ghost.getWidth() / 2);
			int ghostY = (int) (camera.getY() + mouseY - ghost.getHeight() / 2);

			ghost.setX(ghostX);
			ghost.setY(ghostY);
			
			towerManager.setGhostBuildable(!towerManager.canBuildAt(ghost, ghostX, ghostY));

			if (mousePrimaryPressed && !recentlySelected)
			{
				towerManager.build(towerManager.getGhostType(), ghostX, ghostY);
			}
		}
	}

	@Override
	public int getID()
	{
		return STATE.getID();
	}

}
