package com.supericy.state;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


public class LevelEditor extends BasicGameState
{
	
	public static final GameState STATE = GameState.LEVEL_EDITOR;

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException
	{

	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException
	{

	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException
	{

	}

	@Override
	public int getID()
	{
		return STATE.getID();
	}

}
