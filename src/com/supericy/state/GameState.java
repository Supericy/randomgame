package com.supericy.state;

public enum GameState
{

	MAIN_MENU(0),
	MAIN_GAME(1), 
	LEVEL_EDITOR(2);
	
	private int stateID;
	
	private GameState(int stateID)
	{
		this.stateID = stateID;
	}
	
	public int getID()
	{
		return stateID;
	}
	
}
