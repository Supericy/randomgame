package com.supericy.state;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.supericy.gui.Button;
import com.supericy.gui.ButtonAdapter;
import com.supericy.gui.TextButton;
import com.supericy.gui.TextButtonAdapter;

public class MainMenu extends BasicGameState
{

	public static final GameState	STATE				= GameState.MAIN_MENU;

	private static final Color		BUTTON_NORMAL_COLOR	= Color.white;
	private static final Color		BUTTON_HOVER_COLOR	= Color.yellow;

	private float					x;
	private float					y;

	private float					bottomOfMenu;

	private List<TextButton>		buttons;

	public MainMenu()
	{
		x = 200;
		y = 100;
		bottomOfMenu = 0;
		buttons = new ArrayList<TextButton>();
	}

	@Override
	public void init(final GameContainer gc, final StateBasedGame game) throws SlickException
	{
		java.awt.Font font = new java.awt.Font("Trebuchet", java.awt.Font.BOLD, 30);

		Font f = new TrueTypeFont(font, true);

		TextButton playButton = TextButton.factory(f, "Play", BUTTON_NORMAL_COLOR);
		TextButton levelEditorButton = TextButton.factory(f, "Level Editor", BUTTON_NORMAL_COLOR);
		TextButton exitButton = TextButton.factory(f, "Exit", BUTTON_NORMAL_COLOR);

		TextButtonAdapter onHoverColorChange = new TextButtonAdapter() {
			public void onHoverEnter(TextButton b)
			{
				b.setColor(BUTTON_HOVER_COLOR);
			}

			public void onHoverLeave(TextButton b)
			{
				b.setColor(BUTTON_NORMAL_COLOR);
			}
		};

		playButton.addListener(onHoverColorChange);
		playButton.addListener(new TextButtonAdapter() {
			public void onPrimaryMouseUp(TextButton b)
			{
				game.enterState(GameState.MAIN_GAME.getID());
			}
		});

		levelEditorButton.addListener(onHoverColorChange);
		levelEditorButton.addListener(new ButtonAdapter() {
			public void onPrimaryMouseUp(Button b)
			{
				game.enterState(GameState.LEVEL_EDITOR.getID());
			}
		});

		exitButton.addListener(onHoverColorChange);
		exitButton.addListener(new ButtonAdapter() {
			public void onPrimaryMouseUp(Button b)
			{
				gc.exit();
			}
		});

		addButton(playButton);
		addButton(levelEditorButton);
		addButton(exitButton);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException
	{
		for (TextButton button : buttons)
			button.render(gc, g);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int dt) throws SlickException
	{
		for (TextButton button : buttons)
			button.update(gc, dt);
	}

	@Override
	public int getID()
	{
		return STATE.getID();
	}

	private void addButton(TextButton button)
	{
		button.setX(x);
		button.setY(y + bottomOfMenu);

		bottomOfMenu += button.getHeight();

		buttons.add(button);
	}
}
