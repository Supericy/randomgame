package com.supericy.state;

import org.newdawn.slick.geom.Vector2f;

public abstract class AbstractEntity
{

	protected Vector2f position;
	
	public AbstractEntity(Vector2f position)
	{
		this.position = position;
	}
	
	public AbstractEntity(float x, float y)
	{
		this(new Vector2f(x, y));
	}
	
}
