package com.supericy;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.supericy.state.MainGame;

public class TowerDefense extends StateBasedGame
{
	public static final String		VERSION			= "0.0.1";
	public static final String		GAME_NAME		= "Tower Defense";

	public static final int			MAX_FRAMERATE	= 60;

	public static final int			WIDTH			= 800;
	public static final int			HEIGHT			= 600;

	private static AppGameContainer	app;

	public TowerDefense()
	{
		super(String.format("%s %s", GAME_NAME, VERSION));
	}

	public void initStatesList(GameContainer gc) throws SlickException
	{
//		addState(new MainMenu());
		addState(new MainGame());
//		addState(new LevelEditor());
	}

//	protected void postUpdateState(GameContainer gc, int dt) throws SlickException
//	{
//		app.setTitle(String.format("%s %s %s %s", GAME_NAME, VERSION, getCurrentState(), gc.getFPS()));
//	}

	public static void main(String[] args)
	{
		try
		{
			app = new AppGameContainer(new TowerDefense());

			app.setDisplayMode(WIDTH, HEIGHT, false);
			app.setTargetFrameRate(MAX_FRAMERATE);
			app.setShowFPS(true);
			
			app.start();
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
	}

	public static AppGameContainer getApp()
	{
		return app;
	}
}
