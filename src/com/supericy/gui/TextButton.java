package com.supericy.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class TextButton extends Button
{

	private Font				font;
	private String				text;
	private Color				color;

	public TextButton(float x, float y, int width, int height, Font font, String text, Color color)
	{
		super(x, y, width, height);

		this.font = font;
		this.text = text;
		this.color = color;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	public void render(GameContainer gc, Graphics g)
	{
		font.drawString(x, y, text, color);
	}

	public static TextButton factory(Font f, String text, Color color)
	{
		return new TextButton(0, 0, f.getWidth(text), f.getHeight(text), f, text, color);
	}

}
