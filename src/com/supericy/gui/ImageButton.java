package com.supericy.gui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class ImageButton extends Button
{

	private Image	background;

	public ImageButton(float x, float y, int width, int height, Image background)
	{
		super(x, y, width, height);
		
		this.background = background;
	}
	
	@Override
	public void render(GameContainer gc, Graphics g)
	{
		background.draw(x, y, width, height);
	}

}
