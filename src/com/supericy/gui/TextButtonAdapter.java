package com.supericy.gui;

public class TextButtonAdapter implements ButtonListener<TextButton>
{

	@Override
	public void onHoverEnter(TextButton b)
	{
	}

	@Override
	public void onHoverLeave(TextButton b)
	{
	}

	@Override
	public void onPrimaryMouseDown(TextButton b)
	{
	}

	@Override
	public void onPrimaryMouseUp(TextButton b)
	{
	}

	@Override
	public void onPrimaryMouseClick(TextButton b)
	{
	}

}
