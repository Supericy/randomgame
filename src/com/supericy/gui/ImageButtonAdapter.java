package com.supericy.gui;

public class ImageButtonAdapter implements ButtonListener<ImageButton>
{

	@Override
	public void onHoverEnter(ImageButton b)
	{
	}

	@Override
	public void onHoverLeave(ImageButton b)
	{
	}

	@Override
	public void onPrimaryMouseDown(ImageButton b)
	{
	}

	@Override
	public void onPrimaryMouseUp(ImageButton b)
	{
	}

	@Override
	public void onPrimaryMouseClick(ImageButton b)
	{
	}

}
