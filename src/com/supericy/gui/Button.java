package com.supericy.gui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Button
{

	protected float				x;
	protected float				y;

	protected int					width;
	protected int					height;

	private ListenerCollection	listeners	= new ListenerCollection(this);

	private boolean				hovering	= false;
	private boolean				mousePressed;

	public Button(float x, float y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public boolean contains(float x, float y)
	{
		return ((x >= this.x && x < (this.x + width)) && (y >= this.y && y < (this.y + height)));
	}

	public void addListener(ButtonListener<? extends Button> listener)
	{
		listeners.addListener(listener);
	}

	public void render(GameContainer gc, Graphics g)
	{

	}

	public void update(GameContainer gc, int dt)
	{
		Input input = gc.getInput();

		float mouseX = input.getAbsoluteMouseX();
		float mouseY = input.getAbsoluteMouseY();

		if (contains(mouseX, mouseY))
		{
			if (!hovering)
			{
				listeners.dispatch("onHoverEnter");

				hovering = true;
			}

			if (!mousePressed && input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON))
			{
				listeners.dispatch("onPrimaryMouseDown");

				mousePressed = true;
			}
			else if (mousePressed && !input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON))
			{
				listeners.dispatch("onPrimaryMouseUp");

				mousePressed = false;
			}
		}
		else
		{
			if (hovering)
			{
				listeners.dispatch("onHoverLeave");

				hovering = false;
			}

			// mouse isn't on the button anymore so don't fire mouseup event
			mousePressed = false;
		}
	}

}
