package com.supericy.gui;

public interface ButtonListener<T extends Button>
{
	
	public void onHoverEnter(T b);
	public void onHoverLeave(T b);
	
	public void onPrimaryMouseDown(T b);
	public void onPrimaryMouseUp(T b);
	
	public void onPrimaryMouseClick(T b);
	
}
