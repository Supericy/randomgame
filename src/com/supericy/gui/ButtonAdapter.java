package com.supericy.gui;

public class ButtonAdapter implements ButtonListener<Button>
{

	@Override
	public void onHoverEnter(Button b)
	{
	}

	@Override
	public void onHoverLeave(Button b)
	{
	}

	@Override
	public void onPrimaryMouseDown(Button b)
	{
	}

	@Override
	public void onPrimaryMouseUp(Button b)
	{
	}

	@Override
	public void onPrimaryMouseClick(Button b)
	{
	}

}
