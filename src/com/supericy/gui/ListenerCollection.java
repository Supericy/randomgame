package com.supericy.gui;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ListenerCollection
{

	private List<ListenerContainer>	listeners	= new ArrayList<ListenerContainer>();
	private Button					source;
	private Class<?>				sourceClass;

	public ListenerCollection(Button source)
	{
		this.source = source;
	}

	public void addListener(ButtonListener<? extends Button> listener)
	{
		listeners.add(new ListenerContainer(listener.getClass(), listener));
	}

	public void dispatch(String methodName)
	{
		// System.out.println("Dispatching: " + methodName);

		for (ListenerContainer lc : listeners)
			lc.invokeMethod(methodName, source);
	}

	private class ListenerContainer
	{
		public Class<?>							cls;
		public ButtonListener<? extends Button>	listener;
		public Method							method;

		public ListenerContainer(Class<?> cls, ButtonListener<? extends Button> listener)
		{
			this.cls = cls;
			this.listener = listener;
		}

		public void invokeMethod(String methodName, Button source)
		{
			Class<?> srcClass;
			
			for (srcClass = source.getClass(); srcClass != null; srcClass = srcClass.getSuperclass())
			{
				try
				{
					method = cls.getMethod(methodName, srcClass);
					method.setAccessible(true);
					method.invoke(listener, source);
				}
				catch (Exception e)
				{
//					 e.printStackTrace();
				}
			}
		}
	}

}
