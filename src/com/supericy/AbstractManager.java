package com.supericy;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractManager<T>
{
	
	protected final List<T> entities = new ArrayList<T>();
	
	protected final AbstractFactory<T> factory;
	
	public AbstractManager(AbstractFactory<T> factory)
	{
		this.factory = factory;
	}
	
}
